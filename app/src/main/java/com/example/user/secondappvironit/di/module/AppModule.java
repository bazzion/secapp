package com.example.user.secondappvironit.di.module;

import android.app.Application;
import com.example.user.secondappvironit.di.annotation.ApplicationScope;
import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

  private Application mApplication;

  AppModule(Application mApplication) {
    this.mApplication = mApplication;
  }

  @Provides
  @ApplicationScope
  Application provideApplication() {
    return mApplication;
  }
}
