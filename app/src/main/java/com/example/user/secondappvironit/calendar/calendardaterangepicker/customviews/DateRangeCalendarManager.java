package com.example.user.secondappvironit.calendar.calendardaterangepicker.customviews;

import androidx.annotation.IntDef;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

class DateRangeCalendarManager {

  private final static String DATE_FORMAT = "yyyyMMdd";
  public static SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT,
      Locale.getDefault());
  private Calendar minSelectedDate, maxSelectedDate;


  public DateRangeCalendarManager() {
  }

  public Calendar getMaxSelectedDate() {
    return maxSelectedDate;
  }

  public void setMaxSelectedDate(Calendar maxSelectedDate) {
    this.maxSelectedDate = maxSelectedDate;
  }

  public Calendar getMinSelectedDate() {
    return minSelectedDate;
  }

  public void setMinSelectedDate(Calendar minSelectedDate) {
    this.minSelectedDate = minSelectedDate;
  }


  @RANGE_TYPE
  public int checkDateRange(Calendar selectedDate) {

    String dateStr = SIMPLE_DATE_FORMAT.format(selectedDate.getTime());

    if (minSelectedDate != null && maxSelectedDate == null) {
      String minDateStr = SIMPLE_DATE_FORMAT.format(minSelectedDate.getTime());
      if (dateStr.equalsIgnoreCase(minDateStr)) {
        return RANGE_TYPE.START_DATE;
      } else {
        return RANGE_TYPE.NOT_IN_RANGE;
      }
    } else if (minSelectedDate != null) {
      //Min date and Max date are selected
      long selectedDateVal = Long.valueOf(dateStr);

      String minDateStr = SIMPLE_DATE_FORMAT.format(minSelectedDate.getTime());
      String maxDateStr = SIMPLE_DATE_FORMAT.format(maxSelectedDate.getTime());

      long minDateVal = Long.valueOf(minDateStr);
      long maxDateVal = Long.valueOf(maxDateStr);

      if (selectedDateVal == minDateVal) {
        return RANGE_TYPE.START_DATE;
      } else if (selectedDateVal == maxDateVal) {
        return RANGE_TYPE.LAST_DATE;
      } else if (selectedDateVal > minDateVal && selectedDateVal < maxDateVal) {
        return RANGE_TYPE.MIDDLE_DATE;
      } else {
        return RANGE_TYPE.NOT_IN_RANGE;
      }

    } else {
      return RANGE_TYPE.NOT_IN_RANGE;
    }

  }

  @Retention(RetentionPolicy.SOURCE)
  @IntDef({
      RANGE_TYPE.NOT_IN_RANGE, RANGE_TYPE.START_DATE, RANGE_TYPE.MIDDLE_DATE, RANGE_TYPE.LAST_DATE})
  public @interface RANGE_TYPE {

    int NOT_IN_RANGE = 0;
    int START_DATE = 1;
    int MIDDLE_DATE = 2;
    int LAST_DATE = 3;
  }
}
