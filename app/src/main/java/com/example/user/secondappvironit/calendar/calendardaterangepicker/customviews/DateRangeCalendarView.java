package com.example.user.secondappvironit.calendar.calendardaterangepicker.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.viewpager.widget.ViewPager;
import com.example.user.secondappvironit.R;
import com.example.user.secondappvironit.calendar.calendardaterangepicker.models.CalendarStyleAttr;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class DateRangeCalendarView extends LinearLayout {

  private final static int TOTAL_ALLOWED_MONTHS = 30;
  private CustomTextView tvYearTitle;
  private AppCompatImageView imgVNavLeft, imgVNavRight;
  private List<Calendar> monthDataList = new ArrayList<>();

  private AdapterEventCalendarMonths adapterEventCalendarMonths;
  private Locale locale;

  private ViewPager vpCalendar;
  private CalendarStyleAttr calendarStyleAttr;
  private CalendarListener mCalendarListener;


  public DateRangeCalendarView(Context context) {
    super(context);
    initViews(context, null);
  }

  public DateRangeCalendarView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    initViews(context, attrs);
  }

  public DateRangeCalendarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    initViews(context, attrs);
  }

  private void initViews(Context context, AttributeSet attrs) {

    locale = context.getResources().getConfiguration().locale;
    calendarStyleAttr = new CalendarStyleAttr(context, attrs);

    LayoutInflater layoutInflater = LayoutInflater.from(context);
    layoutInflater.inflate(R.layout.layout_calendar_container, this, true);

    RelativeLayout rlHeaderCalendar = findViewById(R.id.rlHeaderCalendar);
    rlHeaderCalendar.setBackground(calendarStyleAttr.getHeaderBg());

    tvYearTitle = findViewById(R.id.tvYearTitle);
    tvYearTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, calendarStyleAttr.getTextSizeTitle());

    imgVNavLeft = findViewById(R.id.imgVNavLeft);
    imgVNavRight = findViewById(R.id.imgVNavRight);

    vpCalendar = findViewById(R.id.vpCalendar);

    monthDataList.clear();
    Calendar today = (Calendar) Calendar.getInstance().clone();
    today.add(Calendar.MONTH, -TOTAL_ALLOWED_MONTHS);

    for (int i = 0; i < TOTAL_ALLOWED_MONTHS * 2; i++) {
      monthDataList.add((Calendar) today.clone());
      today.add(Calendar.MONTH, 1);
    }

    adapterEventCalendarMonths = new AdapterEventCalendarMonths(context, monthDataList,
        calendarStyleAttr);
    vpCalendar.setAdapter(adapterEventCalendarMonths);
    vpCalendar.setOffscreenPageLimit(0);
    vpCalendar.setCurrentItem(TOTAL_ALLOWED_MONTHS);
    setCalendarYearTitle(TOTAL_ALLOWED_MONTHS);

    setListeners();
  }

  private void setListeners() {

    vpCalendar.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

      }

      @Override
      public void onPageSelected(int position) {
        setCalendarYearTitle(position);
        setNavigationHeader(position);
      }

      @Override
      public void onPageScrollStateChanged(int state) {

      }
    });

    imgVNavLeft.setOnClickListener(view -> {
      int newPosition = vpCalendar.getCurrentItem() - 1;
      if (newPosition > -1) {
        vpCalendar.setCurrentItem(newPosition);
      }
    });
    imgVNavRight.setOnClickListener(view -> {
      int newPosition = vpCalendar.getCurrentItem() + 1;
      if (newPosition < monthDataList.size()) {
        vpCalendar.setCurrentItem(newPosition);
      }
    });
  }


  private void setNavigationHeader(int position) {
    imgVNavRight.setVisibility(VISIBLE);
    imgVNavLeft.setVisibility(VISIBLE);
    if (monthDataList.size() == 1) {
      imgVNavLeft.setVisibility(INVISIBLE);
      imgVNavRight.setVisibility(INVISIBLE);
    } else if (position == 0) {
      imgVNavLeft.setVisibility(INVISIBLE);
    } else if (position == monthDataList.size() - 1) {
      imgVNavRight.setVisibility(INVISIBLE);
    }
  }


  private void setCalendarYearTitle(int position) {

    Calendar currentCalendarMonth = monthDataList.get(position);
    String dateText = new DateFormatSymbols(locale).getMonths()[currentCalendarMonth
        .get(Calendar.MONTH)];
    dateText = dateText.substring(0, 1).toUpperCase() + dateText.subSequence(1, dateText.length());

    String yearTitle = dateText + " " + currentCalendarMonth.get(Calendar.YEAR);

    tvYearTitle.setText(yearTitle);
    tvYearTitle.setTextColor(calendarStyleAttr.getTitleColor());

  }


  public void setCalendarListener(final CalendarListener calendarListener) {
    mCalendarListener = calendarListener;
    adapterEventCalendarMonths.setCalendarListener(mCalendarListener);
  }


  public void setFonts(Typeface fonts) {
    tvYearTitle.setTypeface(fonts);
    calendarStyleAttr.setFonts(fonts);
    adapterEventCalendarMonths.invalidateCalendar();
  }


  public void resetAllSelectedViews() {
    adapterEventCalendarMonths.resetAllSelectedViews();
  }







  public void setSelectedDateRange(@Nullable Calendar startDate, @Nullable Calendar endDate) {
    if (startDate == null && endDate != null) {
      throw new RuntimeException("Start date can not be null if you are setting end date.");
    } else if (endDate != null && endDate.before(startDate)) {
      throw new RuntimeException("Start date can not be after end date.");
    }
    adapterEventCalendarMonths.setSelectedDate(startDate, endDate);
  }


  public Calendar getStartDate() {
    return adapterEventCalendarMonths.getMinSelectedDate();
  }


  public Calendar getEndDate() {
    return adapterEventCalendarMonths.getMaxSelectedDate();
  }



  public boolean isEditable() {
    return adapterEventCalendarMonths.isEditable();
  }


  public void setEditable(boolean isEditable) {
    adapterEventCalendarMonths.setEditable(isEditable);
  }


  public void setVisibleMonthRange(Calendar startMonth, Calendar endMonth) {

    if (startMonth == null) {
      throw new IllegalArgumentException("Start month can not be null.");
    }
    startMonth.set(Calendar.DATE, 1);
    startMonth.set(Calendar.HOUR, 0);
    startMonth.set(Calendar.MINUTE, 0);
    startMonth.set(Calendar.SECOND, 0);
    startMonth.set(Calendar.MILLISECOND, 0);

    if (endMonth == null) {
      throw new IllegalArgumentException("End month can not be null.");
    }
    endMonth.set(Calendar.DATE, 1);
    endMonth.set(Calendar.HOUR, 0);
    endMonth.set(Calendar.MINUTE, 0);
    endMonth.set(Calendar.SECOND, 0);
    endMonth.set(Calendar.MILLISECOND, 0);

    if (startMonth.after(endMonth)) {
      throw new IllegalArgumentException("Start month can not be greater than end month.");
    }
    monthDataList.clear();

    do {
      monthDataList.add((Calendar) startMonth.clone());
      startMonth.add(Calendar.MONTH, 1);
    }
    while (startMonth.compareTo(endMonth) != 0);

    adapterEventCalendarMonths = new AdapterEventCalendarMonths(getContext(), monthDataList,
        calendarStyleAttr);
    vpCalendar.setAdapter(adapterEventCalendarMonths);
    vpCalendar.setOffscreenPageLimit(0);
    vpCalendar.setCurrentItem(0);
    setCalendarYearTitle(0);
    setNavigationHeader(0);
    adapterEventCalendarMonths.setCalendarListener(mCalendarListener);

  }


  public void setCurrentMonth(Calendar calendar) {
    if (calendar != null && monthDataList != null) {
      for (int i = 0; i < monthDataList.size(); i++) {
        Calendar month = monthDataList.get(i);
        if (month.get(Calendar.MONTH) == calendar.get(Calendar.MONTH)) {
          vpCalendar.setCurrentItem(i);
          break;
        }
      }
    }
  }

  public interface CalendarListener {

    void onFirstDateSelected(Calendar startDate);

    void onDateRangeSelected(Calendar startDate, Calendar endDate);
  }
}
