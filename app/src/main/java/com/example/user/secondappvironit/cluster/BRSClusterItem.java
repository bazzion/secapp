package com.example.user.secondappvironit.cluster;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class BRSClusterItem implements ClusterItem {

  private final LatLng mPosition;
  private final String mTitle;
  private final String mSnippet;
  private final int mId;


  public BRSClusterItem(double lat, double lng, String title, String snippet, int id) {
    mPosition = new LatLng(lat, lng);
    mTitle = title;
    mSnippet = snippet;
    mId = id;

  }

  @Override
  public LatLng getPosition() {
    return mPosition;
  }

  @Override
  public String getTitle() {
    return mTitle;
  }

  @Override
  public String getSnippet() {
    return mSnippet;
  }

  public int getId() {
    return mId;
  }
}
