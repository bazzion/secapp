package com.example.user.secondappvironit.cluster;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.TextView;
import com.example.user.secondappvironit.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

public class CustomClusterRenderer extends DefaultClusterRenderer<BRSClusterItem> {

  private final IconGenerator mClusterIconGenerator;
  private final Context mContext;


  public CustomClusterRenderer(Context context,
      GoogleMap map,
      ClusterManager clusterManager) {
    super(context, map, clusterManager);

    mContext = context;

    mClusterIconGenerator = new IconGenerator(context.getApplicationContext());
  }

  @SuppressLint("SetTextI18n")
  @Override
  protected void onBeforeClusterItemRendered(BRSClusterItem brsClusterItem,
      MarkerOptions markerOptions) {

    View customIconView = View.inflate(mContext, R.layout.custom_marker, null);
    TextView markerTextView = customIconView.findViewById(R.id.markerIdTextView);
    markerTextView.setText(Integer.toString(brsClusterItem.getId()));
    mClusterIconGenerator.setBackground(mContext.getDrawable(R.drawable.bg_marker));
    mClusterIconGenerator.setContentView(customIconView);
    final Bitmap icon = mClusterIconGenerator.makeIcon();
    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));

  }
}
