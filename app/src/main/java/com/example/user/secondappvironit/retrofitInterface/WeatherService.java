package com.example.user.secondappvironit.retrofitInterface;

import com.example.user.secondappvironit.pojo.weatherModel.WeatherResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {

  @GET("data/2.5/weather?APPID=6a230e4949c456d8e49596266657dbaf&units=metric&lang=ru")
  Observable<WeatherResponse> getCurrentWeatherData(@Query("q") String city);

}
