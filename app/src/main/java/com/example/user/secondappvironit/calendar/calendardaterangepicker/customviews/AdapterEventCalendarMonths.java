package com.example.user.secondappvironit.calendar.calendardaterangepicker.customviews;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import com.example.user.secondappvironit.R;
import com.example.user.secondappvironit.calendar.calendardaterangepicker.models.CalendarStyleAttr;
import java.util.Calendar;
import java.util.List;

public class AdapterEventCalendarMonths extends PagerAdapter {

  private Context mContext;
  private List<Calendar> dataList;
  private CalendarStyleAttr calendarStyleAttr;
  private DateRangeCalendarView.CalendarListener calendarListener;
  private DateRangeCalendarManager dateRangeCalendarManager;
  private Handler mHandler;
  private DateRangeCalendarView.CalendarListener calendarAdapterListener = new DateRangeCalendarView.CalendarListener() {
    @Override
    public void onFirstDateSelected(Calendar startDate) {

      mHandler.postDelayed(() -> notifyDataSetChanged(), 50);

      if (calendarListener != null) {
        calendarListener.onFirstDateSelected(startDate);
      }
    }

    @Override
    public void onDateRangeSelected(Calendar startDate, Calendar endDate) {
      mHandler.postDelayed(() -> notifyDataSetChanged(), 50);
      if (calendarListener != null) {
        calendarListener.onDateRangeSelected(startDate, endDate);
      }
    }
  };

  public AdapterEventCalendarMonths(Context mContext, List<Calendar> list,
      CalendarStyleAttr calendarStyleAttr) {
    this.mContext = mContext;
    dataList = list;
    this.calendarStyleAttr = calendarStyleAttr;
    dateRangeCalendarManager = new DateRangeCalendarManager();
    mHandler = new Handler();
  }

  @Override
  public int getCount() {
    return dataList.size();
  }

  @Override
  public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
    return view == object;
  }

  @NonNull
  @Override
  public Object instantiateItem(@NonNull ViewGroup container, int position) {

    Calendar modelObject = dataList.get(position);
    LayoutInflater inflater = LayoutInflater.from(mContext);
    ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_pager_month, container, false);

    DateRangeMonthView dateRangeMonthView = layout.findViewById(R.id.cvEventCalendarView);
    dateRangeMonthView.drawCalendarForMonth(calendarStyleAttr, getCurrentMonth(modelObject),
        dateRangeCalendarManager);
    dateRangeMonthView.setCalendarListener(calendarAdapterListener);

    container.addView(layout);
    return layout;
  }


  private Calendar getCurrentMonth(Calendar calendar) {
    Calendar current = (Calendar) calendar.clone();
    current.set(Calendar.DAY_OF_MONTH, 1);
    return current;
  }

  @Override
  public void destroyItem(@NonNull ViewGroup collection, int position, @NonNull Object view) {
    collection.removeView((View) view);
  }

  @Override
  public int getItemPosition(Object object) {
    return POSITION_NONE;
  }

  public void setCalendarListener(DateRangeCalendarView.CalendarListener calendarListener) {
    this.calendarListener = calendarListener;
  }

  public void invalidateCalendar() {
    mHandler.postDelayed(() -> notifyDataSetChanged(), 50);
  }


  public void resetAllSelectedViews() {
    dateRangeCalendarManager.setMinSelectedDate(null);
    dateRangeCalendarManager.setMaxSelectedDate(null);
    notifyDataSetChanged();
  }


  public void setSelectedDate(Calendar minSelectedDate, Calendar maxSelectedDate) {
    dateRangeCalendarManager.setMinSelectedDate(minSelectedDate);
    dateRangeCalendarManager.setMaxSelectedDate(maxSelectedDate);
    notifyDataSetChanged();
  }

  public Calendar getMinSelectedDate() {
    return dateRangeCalendarManager.getMinSelectedDate();
  }

  public Calendar getMaxSelectedDate() {
    return dateRangeCalendarManager.getMaxSelectedDate();
  }


  public boolean isEditable() {
    return calendarStyleAttr.isEditable();
  }


  public void setEditable(boolean isEditable) {
    calendarStyleAttr.setEditable(isEditable);
    notifyDataSetChanged();
  }
}
