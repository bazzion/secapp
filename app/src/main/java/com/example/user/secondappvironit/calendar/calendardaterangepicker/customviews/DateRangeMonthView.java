package com.example.user.secondappvironit.calendar.calendardaterangepicker.customviews;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.annotation.ColorInt;
import androidx.core.content.ContextCompat;
import com.example.user.secondappvironit.R;
import com.example.user.secondappvironit.calendar.calendardaterangepicker.models.CalendarStyleAttr;
import com.example.user.secondappvironit.calendar.calendardaterangepicker.models.DayContainer;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

class DateRangeMonthView extends LinearLayout {


  private final static PorterDuff.Mode FILTER_MODE = PorterDuff.Mode.SRC_IN;
  private LinearLayout llDaysContainer;
  private LinearLayout llTitleWeekContainer;
  private Calendar currentCalendarMonth;
  private CalendarStyleAttr calendarStyleAttr;
  private DateRangeCalendarView.CalendarListener calendarListener;
  private DateRangeCalendarManager dateRangeCalendarManager;
  private OnClickListener dayClickListener = new OnClickListener() {
    @Override
    public void onClick(View view) {

      if (calendarStyleAttr.isEditable()) {
        int key = (int) view.getTag();
        final Calendar selectedCal = Calendar.getInstance();
        Date date = new Date();
        try {
          date = DateRangeCalendarManager.SIMPLE_DATE_FORMAT.parse(String.valueOf(key));
        } catch (ParseException e) {
          e.printStackTrace();
        }
        selectedCal.setTime(date);

        Calendar minSelectedDate = dateRangeCalendarManager.getMinSelectedDate();
        Calendar maxSelectedDate = dateRangeCalendarManager.getMaxSelectedDate();

        if (minSelectedDate != null && maxSelectedDate == null) {
          maxSelectedDate = selectedCal;

          int startDateKey = DayContainer.GetContainerKey(minSelectedDate);
          int lastDateKey = DayContainer.GetContainerKey(maxSelectedDate);

          if (startDateKey == lastDateKey) {
            minSelectedDate = maxSelectedDate;
          } else if (startDateKey > lastDateKey) {
            Calendar temp = (Calendar) minSelectedDate.clone();
            minSelectedDate = maxSelectedDate;
            maxSelectedDate = temp;
          }
        } else if (maxSelectedDate == null) {
          minSelectedDate = selectedCal;
        } else {
          minSelectedDate = selectedCal;
          maxSelectedDate = null;
        }

        dateRangeCalendarManager.setMinSelectedDate(minSelectedDate);
        dateRangeCalendarManager.setMaxSelectedDate(maxSelectedDate);
        drawCalendarForMonth(currentCalendarMonth);

//        if (calendarStyleAttr.isShouldEnabledTime()) {
//          final Calendar finalMinSelectedDate = minSelectedDate;
//          final Calendar finalMaxSelectedDate = maxSelectedDate;
//          AwesomeTimePickerDialog awesomeTimePickerDialog = new AwesomeTimePickerDialog(
//              getContext(), getContext().getString(R.string.select_time),
//              new AwesomeTimePickerDialog.TimePickerCallback() {
//                @Override
//                public void onTimeSelected(int hours, int mins) {
//                  selectedCal.set(Calendar.HOUR, hours);
//                  selectedCal.set(Calendar.MINUTE, mins);
//
//                  Log.i(LOG_TAG, "Time: " + selectedCal.getTime().toString());
//                  if (calendarListener != null) {
//
//                    if (finalMaxSelectedDate != null) {
//                      calendarListener
//                          .onDateRangeSelected(finalMinSelectedDate, finalMaxSelectedDate);
//                    } else {
//                      calendarListener.onFirstDateSelected(finalMinSelectedDate);
//                    }
//                  }
//                }
//
//                @Override
//                public void onCancel() {
//                  DateRangeMonthView.this.resetAllSelectedViews();
//                }
//              });
//          awesomeTimePickerDialog.showDialog();
//        } else {
//          Log.i(LOG_TAG, "Time: " + selectedCal.getTime().toString());
//          if (maxSelectedDate != null) {
//            calendarListener.onDateRangeSelected(minSelectedDate, maxSelectedDate);
//          } else {
//            calendarListener.onFirstDateSelected(minSelectedDate);
//          }
//        }
      }
    }
  };

  public DateRangeMonthView(Context context) {
    super(context);
    initView(context, null);
  }

  public DateRangeMonthView(Context context, AttributeSet attrs) {
    super(context, attrs);
    initView(context, attrs);
  }

  public DateRangeMonthView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    initView(context, attrs);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public DateRangeMonthView(Context context, AttributeSet attrs, int defStyleAttr,
      int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
    initView(context, attrs);
  }

  public void setCalendarListener(DateRangeCalendarView.CalendarListener calendarListener) {
    this.calendarListener = calendarListener;
  }

  private void initView(Context context, AttributeSet attributeSet) {
    LayoutInflater layoutInflater = LayoutInflater.from(context);
    LinearLayout mainView = (LinearLayout) layoutInflater
        .inflate(R.layout.layout_calendar_month, this, true);
    llDaysContainer = mainView.findViewById(R.id.llDaysContainer);
    llTitleWeekContainer = mainView.findViewById(R.id.llTitleWeekContainer);

    setListeners();

    if (isInEditMode()) {
      return;
    }

  }

  private void setListeners() {

  }

  public void drawCalendarForMonth(CalendarStyleAttr calendarStyleAttr, Calendar month,
      DateRangeCalendarManager dateRangeCalendarManager) {
    this.calendarStyleAttr = calendarStyleAttr;
    this.currentCalendarMonth = (Calendar) month.clone();
    this.dateRangeCalendarManager = dateRangeCalendarManager;
    setConfigs();
    setWeekTitleColor(calendarStyleAttr.getWeekColor());
    drawCalendarForMonth(currentCalendarMonth);
  }


  private void drawCalendarForMonth(Calendar month) {

    currentCalendarMonth = (Calendar) month.clone();
    currentCalendarMonth.set(Calendar.DATE, 1);
    currentCalendarMonth.set(Calendar.HOUR, 0);
    currentCalendarMonth.set(Calendar.MINUTE, 0);
    currentCalendarMonth.set(Calendar.SECOND, 0);

    String[] weekTitle = getContext().getResources().getStringArray(R.array.week_sun_sat);

    for (int i = 0; i < 7; i++) {

      CustomTextView textView = (CustomTextView) llTitleWeekContainer.getChildAt(i);

      String weekStr = weekTitle[(i + calendarStyleAttr.getWeekOffset()) % 7];
      textView.setText(weekStr);

    }

    int startDay = month.get(Calendar.DAY_OF_WEEK) - calendarStyleAttr.getWeekOffset();

    if (startDay < 1) {
      startDay = startDay + 7;
    }

    month.add(Calendar.DATE, -startDay + 1);

    for (int i = 0; i < llDaysContainer.getChildCount(); i++) {
      LinearLayout weekRow = (LinearLayout) llDaysContainer.getChildAt(i);

      for (int j = 0; j < 7; j++) {
        RelativeLayout rlDayContainer = (RelativeLayout) weekRow.getChildAt(j);

        DayContainer container = new DayContainer(rlDayContainer);

        container.tvDate.setText(String.valueOf(month.get(Calendar.DATE)));
        if (calendarStyleAttr.getFonts() != null) {
          container.tvDate.setTypeface(calendarStyleAttr.getFonts());
        }
        drawDayContainer(container, month);
        month.add(Calendar.DATE, 1);
      }
    }
  }


  private void drawDayContainer(DayContainer container, Calendar calendar) {

    Calendar today = Calendar.getInstance();

    int date = calendar.get(Calendar.DATE);

    if (currentCalendarMonth.get(Calendar.MONTH) != calendar.get(Calendar.MONTH)) {
      hideDayContainer(container);
    } else if (today.after(calendar) && (today.get(Calendar.DAY_OF_YEAR) != calendar
        .get(Calendar.DAY_OF_YEAR))
        && !calendarStyleAttr.isEnabledPastDates()) {
      disableDayContainer(container);
      container.tvDate.setText(String.valueOf(date));
    } else {
      @DateRangeCalendarManager.RANGE_TYPE
      int type = dateRangeCalendarManager.checkDateRange(calendar);
      if (type == DateRangeCalendarManager.RANGE_TYPE.START_DATE
          || type == DateRangeCalendarManager.RANGE_TYPE.LAST_DATE) {
        makeAsSelectedDate(container, type);
      } else if (type == DateRangeCalendarManager.RANGE_TYPE.MIDDLE_DATE) {
        makeAsRangeDate(container);
      } else {
        enabledDayContainer(container);
      }

      container.tvDate.setText(String.valueOf(date));
      container.tvDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, calendarStyleAttr.getTextSizeDate());
    }

    container.rootView.setTag(DayContainer.GetContainerKey(calendar));
  }


  private void hideDayContainer(DayContainer container) {
    container.tvDate.setText("");
    container.tvDate.setBackgroundColor(Color.TRANSPARENT);
    container.strip.setBackgroundColor(Color.TRANSPARENT);
    container.rootView.setBackgroundColor(Color.TRANSPARENT);
    container.rootView.setVisibility(INVISIBLE);
    container.rootView.setOnClickListener(null);
  }


  private void disableDayContainer(DayContainer container) {
    container.tvDate.setBackgroundColor(Color.TRANSPARENT);
    container.strip.setBackgroundColor(Color.TRANSPARENT);
    container.rootView.setBackgroundColor(Color.TRANSPARENT);
    container.tvDate.setTextColor(calendarStyleAttr.getDisableDateColor());
    container.rootView.setVisibility(VISIBLE);
    container.rootView.setOnClickListener(null);
  }

  private void enabledDayContainer(DayContainer container) {
    container.tvDate.setBackgroundColor(Color.TRANSPARENT);
    container.strip.setBackgroundColor(Color.TRANSPARENT);
    container.rootView.setBackgroundColor(Color.TRANSPARENT);
    container.tvDate.setTextColor(calendarStyleAttr.getDefaultDateColor());
    container.rootView.setVisibility(VISIBLE);
    container.rootView.setOnClickListener(dayClickListener);
  }


  private void makeAsSelectedDate(DayContainer container,
      @DateRangeCalendarManager.RANGE_TYPE int stripType) {
    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) container.strip
        .getLayoutParams();

    Calendar minDate = dateRangeCalendarManager.getMinSelectedDate();
    Calendar maxDate = dateRangeCalendarManager.getMaxSelectedDate();

    if (stripType == DateRangeCalendarManager.RANGE_TYPE.START_DATE && maxDate != null &&
        minDate.compareTo(maxDate) != 0) {
      Drawable mDrawable = ContextCompat.getDrawable(getContext(), R.drawable.range_bg_left);
      mDrawable.setColorFilter(
          new PorterDuffColorFilter(calendarStyleAttr.getRangeStripColor(), FILTER_MODE));

      container.strip.setBackground(mDrawable);
      layoutParams.setMargins(20, 0, 0, 0);
    } else if (stripType == DateRangeCalendarManager.RANGE_TYPE.LAST_DATE) {
      Drawable mDrawable = ContextCompat.getDrawable(getContext(), R.drawable.range_bg_right);
      mDrawable.setColorFilter(
          new PorterDuffColorFilter(calendarStyleAttr.getRangeStripColor(), FILTER_MODE));
      container.strip.setBackground(mDrawable);
      layoutParams.setMargins(0, 0, 20, 0);
    } else {
      container.strip.setBackgroundColor(Color.TRANSPARENT);
      layoutParams.setMargins(0, 0, 0, 0);
    }
    container.strip.setLayoutParams(layoutParams);
    Drawable mDrawable = ContextCompat.getDrawable(getContext(), R.drawable.green_circle);
    mDrawable.setColorFilter(
        new PorterDuffColorFilter(calendarStyleAttr.getSelectedDateCircleColor(), FILTER_MODE));
    container.tvDate.setBackground(mDrawable);
    container.rootView.setBackgroundColor(Color.TRANSPARENT);
    container.tvDate.setTextColor(calendarStyleAttr.getSelectedDateColor());
    container.rootView.setVisibility(VISIBLE);
    container.rootView.setOnClickListener(dayClickListener);
  }


  private void makeAsRangeDate(DayContainer container) {
    container.tvDate.setBackgroundColor(Color.TRANSPARENT);
    Drawable mDrawable = ContextCompat.getDrawable(getContext(), R.drawable.range_bg);
    mDrawable.setColorFilter(
        new PorterDuffColorFilter(calendarStyleAttr.getRangeStripColor(), FILTER_MODE));
    container.strip.setBackground(mDrawable);
    container.rootView.setBackgroundColor(Color.TRANSPARENT);
    container.tvDate.setTextColor(calendarStyleAttr.getRangeDateColor());
    container.rootView.setVisibility(VISIBLE);
    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) container.strip
        .getLayoutParams();
    layoutParams.setMargins(0, 0, 0, 0);
    container.strip.setLayoutParams(layoutParams);
    container.rootView.setOnClickListener(dayClickListener);
  }


  public void resetAllSelectedViews() {

    dateRangeCalendarManager.setMinSelectedDate(null);
    dateRangeCalendarManager.setMaxSelectedDate(null);

    drawCalendarForMonth(currentCalendarMonth);

  }


  public void setWeekTitleColor(@ColorInt int color) {
    for (int i = 0; i < llTitleWeekContainer.getChildCount(); i++) {
      CustomTextView textView = (CustomTextView) llTitleWeekContainer.getChildAt(i);
      textView.setTextColor(color);
    }
  }


  private void setConfigs() {

    drawCalendarForMonth(currentCalendarMonth);

    for (int i = 0; i < llTitleWeekContainer.getChildCount(); i++) {

      CustomTextView textView = (CustomTextView) llTitleWeekContainer.getChildAt(i);
      textView.setTypeface(calendarStyleAttr.getFonts());
      textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, calendarStyleAttr.getTextSizeWeek());
    }
  }
}
