package com.example.user.secondappvironit.calendar.calendardaterangepicker.models;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.RelativeLayout;
import com.example.user.secondappvironit.calendar.calendardaterangepicker.customviews.CustomTextView;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class DayContainer {

    public RelativeLayout rootView;
    public CustomTextView tvDate;
    public View strip;

    @SuppressLint("ConstantLocale")
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());

    public DayContainer(RelativeLayout rootView) {
        this.rootView = rootView;
        strip = rootView.getChildAt(0);
        tvDate = (CustomTextView) rootView.getChildAt(1);
    }

    public static int GetContainerKey(Calendar cal) {

        String str = simpleDateFormat.format(cal.getTime());
        int key = Integer.valueOf(str);
        return key;
    }


}
