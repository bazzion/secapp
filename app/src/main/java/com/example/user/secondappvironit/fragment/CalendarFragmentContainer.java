package com.example.user.secondappvironit.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.example.user.secondappvironit.R;
import com.example.user.secondappvironit.calendar.calendardaterangepicker.customviews.DateRangeCalendarView;

public class CalendarFragmentContainer extends Fragment {


  private Unbinder unbinder;
  private DateRangeCalendarView calendar;

  static CalendarFragmentContainer newInstance() {
    return new CalendarFragmentContainer();
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_calendar_container, null);
    calendar = view.findViewById(R.id.mainCalendar);

//    Typeface font = Typeface
//        .createFromAsset(view.getContext().getAssets(), "droid_serif_regular.ttf");
//    calendar.setFonts(font);

    unbinder = ButterKnife.bind(this, view);

    return view;
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setRetainInstance(true);
  }

  @Override
  public void onDestroyView() {
    unbinder.unbind();
    super.onDestroyView();
  }


}
