package com.example.user.secondappvironit.main;

import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.astuetz.PagerSlidingTabStrip;
import com.example.user.secondappvironit.R;
import com.example.user.secondappvironit.fragment.BRSMapFragmentContainer;
import com.example.user.secondappvironit.fragment.MyFragmentPagerAdapter;

public class MainActivity extends FragmentActivity {

  @BindView(R.id.pager)
  ViewPager pager;
  PagerAdapter pagerAdapter;

  private boolean checkZoomMapState = true;
  private final static int pagesLimit = 3;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_activity_main);

    ButterKnife.bind(this);

    pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
    pager.setAdapter(pagerAdapter);
    pager.setOffscreenPageLimit(pagesLimit);

    PagerSlidingTabStrip tabsStrip = findViewById(R.id.tabs);
    tabsStrip.setShouldExpand(true);
    tabsStrip.setViewPager(pager);

    OnPageChangeListener pageChangeListener = new OnPageChangeListener() {
      @Override
      public void onPageScrollStateChanged(int arg0) {
      }

      @Override
      public void onPageScrolled(int arg0, float arg1, int arg2) {
      }

      @Override
      public void onPageSelected(int position) {

        if (position == 2 && checkZoomMapState) {

          BRSMapFragmentContainer.zoomMap();

          checkZoomMapState = false;

        }

      }
    };

    pager.addOnPageChangeListener(pageChangeListener);

    pager.post(() -> pageChangeListener.onPageSelected(pager.getCurrentItem()));


  }


}
