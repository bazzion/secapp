package com.example.user.secondappvironit.di.module;

import android.app.Application;
import com.example.user.secondappvironit.di.component.BRSMapComponent;
import com.example.user.secondappvironit.di.component.DaggerBRSMapComponent;
import com.example.user.secondappvironit.di.component.DaggerWeatherComponent;
import com.example.user.secondappvironit.di.component.WeatherComponent;

public class App extends Application {

  private WeatherComponent mWeatherComponent;
  private BRSMapComponent mBRSMapComponent;

  @Override
  public void onCreate() {
    super.onCreate();

    mWeatherComponent = DaggerWeatherComponent.builder()
        .appModule(new AppModule(this))
        .weatherModule(new WeatherModule())
        .build();

    mBRSMapComponent = DaggerBRSMapComponent.builder()
        .appModule(new AppModule(this))
        .weatherModule(new WeatherModule())
        .build();


  }

  public WeatherComponent getWeatherComponent() {
    return mWeatherComponent;
  }

  public BRSMapComponent getBRSMapComponent() {
    return mBRSMapComponent;
  }

}