package com.example.user.secondappvironit.fragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import com.astuetz.PagerSlidingTabStrip.IconTabProvider;
import com.example.user.secondappvironit.R;

public class MyFragmentPagerAdapter extends FragmentPagerAdapter implements IconTabProvider {

  private int tabTitles[] = new int[]{R.drawable.ic_cloud, R.drawable.ic_calendar, R.drawable.ic_map};


  public MyFragmentPagerAdapter(
      FragmentManager supportFragmentManager) {
    super(supportFragmentManager);
  }


  @NonNull
  @Override
  public Fragment getItem(int position) {
    switch (position) {
      case 0:
        return WeatherFragmentContainer.newInstance();
      case 1:
        return CalendarFragmentContainer.newInstance();
      case 2:
        return BRSMapFragmentContainer.newInstance();
      default:
        return null;
    }
  }

  @Override
  public int getCount() {
    return tabTitles.length;
  }

  @Override
  public int getPageIconResId(int position) {



    return tabTitles[position];


  }


}