package com.example.user.secondappvironit.fragment;

import android.Manifest;
import android.Manifest.permission;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.example.user.secondappvironit.R;
import com.example.user.secondappvironit.cluster.BRSClusterItem;
import com.example.user.secondappvironit.cluster.CustomClusterRenderer;
import com.example.user.secondappvironit.di.module.App;
import com.example.user.secondappvironit.retrofitInterface.BRSMapService;
import com.example.user.secondappvironit.pojo.brsModel.Item;
import com.example.user.secondappvironit.pojo.brsModel.OfficeBRS;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.maps.android.clustering.ClusterManager;
import java.util.List;
import java.util.Objects;
import javax.inject.Inject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class BRSMapFragmentContainer extends Fragment implements OnMapReadyCallback {


  private static GoogleMap googleMap;
  @BindView(R.id.mapBRSView)
  MapView mMapView;
  @Inject
  Retrofit mRetrofit;
  private Unbinder unbinder;


  private ClusterManager<BRSClusterItem> mClusterManager;

  static BRSMapFragmentContainer newInstance() {
    return new BRSMapFragmentContainer();
  }

  public static void zoomMap() {

    googleMap
        .animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(53.893009, 27.567444), 5.6f));

  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {

    @SuppressLint("InflateParams") View view = inflater
        .inflate(R.layout.fragment_map_container, null);

    unbinder = ButterKnife.bind(this, view);

    mMapView.onCreate(savedInstanceState);

    getBRSInfo();

    createMap();

    return view;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {

    ((App) Objects.requireNonNull(getActivity()).getApplication()).getBRSMapComponent()
        .injectBRSMap(this);

    super.onCreate(savedInstanceState);
    setRetainInstance(true);

  }

  private void getBRSInfo() {

    Call<OfficeBRS> brsMapServiceCall = mRetrofit.create(BRSMapService.class).getBRSOfficeData();

    brsMapServiceCall.enqueue(new Callback<OfficeBRS>() {
      @Override
      public void onResponse(Call<OfficeBRS> call, Response<OfficeBRS> response) {

        if (response.isSuccessful()) {

          List<Item> items = Objects.requireNonNull(response.body()).getItems();

          mClusterManager = new ClusterManager<>(Objects.requireNonNull(getContext()), googleMap);

          mClusterManager
              .setRenderer(new CustomClusterRenderer(getContext(), googleMap, mClusterManager));

          googleMap.setOnCameraIdleListener(mClusterManager);
          googleMap.setOnMarkerClickListener(mClusterManager);

          addOfficeItems(items);

          mClusterManager.setOnClusterClickListener(cluster -> {

            googleMap
                .animateCamera(
                    CameraUpdateFactory.newLatLngZoom(cluster.getPosition(), 15));

            return true;
          });

          mClusterManager.setOnClusterItemClickListener(
              brsClusterItem -> {

                googleMap
                    .animateCamera(
                        CameraUpdateFactory.newLatLngZoom(brsClusterItem.getPosition(), 15));
                return true;
              });
        }
      }

      @Override
      public void onFailure(Call<OfficeBRS> call, Throwable t) {

        Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

      }
    });
  }

  private void addOfficeItems(List<Item> items) {

    int sizeItemsBRS = items.size();

    for (int i = 0; i < sizeItemsBRS; i++) {

      BRSClusterItem brsClusterItem = new BRSClusterItem(items.get(i).getLatitude(),
          items.get(i).getLongitude(),
          items.get(i).getCity(), items.get(i).getAddress(), items.get(i).getId());
      mClusterManager.addItem(brsClusterItem);


    }

  }

  private void createMap() {

    mMapView.getMapAsync(this);
  }

  private void moveToCurrentLocation(GoogleMap googleMap) {

    if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
        Manifest.permission.ACCESS_FINE_LOCATION)
        == PackageManager.PERMISSION_GRANTED && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      googleMap.setMyLocationEnabled(true);

      googleMap.setOnMyLocationClickListener(location -> googleMap.animateCamera(CameraUpdateFactory
          .newLatLng(new LatLng(location.getLatitude(), location.getLongitude()))));
    } else {

      this.requestPermissions(new String[]{
          permission.ACCESS_FINE_LOCATION,
          permission.ACCESS_COARSE_LOCATION,
      }, 23);

    }
  }

  @Override
  public void onResume() {
    super.onResume();
    mMapView.onResume();
  }

  @Override
  public void onPause() {
    super.onPause();
    mMapView.onPause();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    if (mMapView != null) {
      mMapView.onDestroy();
    }
  }

  @Override
  public void onLowMemory() {
    super.onLowMemory();
    mMapView.onLowMemory();
  }

  @Override
  public void onDestroyView() {
    unbinder.unbind();
    super.onDestroyView();
  }

  @Override
  public void onMapReady(GoogleMap mMap) {
    googleMap = mMap;

    moveToCurrentLocation(googleMap);

    googleMap.setIndoorEnabled(true);
    UiSettings uiSettings = googleMap.getUiSettings();
    uiSettings.isIndoorLevelPickerEnabled();
    uiSettings.setMapToolbarEnabled(true);
    uiSettings.setCompassEnabled(true);
    uiSettings.setZoomControlsEnabled(true);

    googleMap.setMaxZoomPreference(20.0f);
    googleMap.setMinZoomPreference(2.0f);

    googleMap.setMapStyle(
        MapStyleOptions.loadRawResourceStyle(
            Objects.requireNonNull(getContext()), R.raw.style_json_black));


  }

}
