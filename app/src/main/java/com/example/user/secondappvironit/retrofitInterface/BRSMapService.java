package com.example.user.secondappvironit.retrofitInterface;

import com.example.user.secondappvironit.pojo.brsModel.OfficeBRS;
import retrofit2.Call;
import retrofit2.http.GET;

public interface BRSMapService {

  @GET("http://ais.brs.by:38516/ords/mobile_user/v1_3/GetOfficesInfo/")
  Call<OfficeBRS> getBRSOfficeData();

}
