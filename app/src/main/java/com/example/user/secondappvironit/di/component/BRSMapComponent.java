package com.example.user.secondappvironit.di.component;

import com.example.user.secondappvironit.di.annotation.ApplicationScope;
import com.example.user.secondappvironit.di.module.AppModule;
import com.example.user.secondappvironit.di.module.WeatherModule;
import com.example.user.secondappvironit.fragment.BRSMapFragmentContainer;
import dagger.Component;

@ApplicationScope
@Component(modules = {AppModule.class, WeatherModule.class})
public interface BRSMapComponent {

  void injectBRSMap(BRSMapFragmentContainer BRSMapFragmentContainer);

}
