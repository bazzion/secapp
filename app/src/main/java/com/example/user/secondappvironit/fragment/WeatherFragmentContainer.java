package com.example.user.secondappvironit.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.example.user.secondappvironit.R;
import com.example.user.secondappvironit.di.module.App;
import com.example.user.secondappvironit.pojo.weatherModel.WeatherResponse;
import com.example.user.secondappvironit.retrofitInterface.WeatherService;
import com.google.android.material.snackbar.Snackbar;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.Date;
import java.util.Objects;
import javax.inject.Inject;
import retrofit2.Retrofit;

public class WeatherFragmentContainer extends Fragment {


  @BindView(R.id.cityTextView)
  TextView cityTextView;
  @BindView(R.id.temperatureTextView)
  TextView temperatureTextView;
  @BindView(R.id.uniIconTextView)
  TextView unicodeIconTextView;
  @BindView(R.id.weatherTextView)
  TextView weatherTextView;
  @BindView(R.id.humidityTextView)
  TextView humidityTextView;
  @BindView(R.id.pressureTextView)
  TextView pressureTextView;
  @BindView(R.id.citySearchView)
  SearchView citySearchView;
  @BindView(R.id.searchProgressBar)
  ProgressBar searchProgressBar;


  @Inject
  Retrofit mRetrofit;
  private Unbinder unbinder;
  private String cityName;

  static WeatherFragmentContainer newInstance() {
    return new WeatherFragmentContainer();
  }

  public static boolean isConnectedCheck(Context context) {
    ConnectivityManager
        cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    return activeNetwork != null
        && activeNetwork.isConnectedOrConnecting();
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_weather_container, null);

    unbinder = ButterKnife.bind(this, view);

    getCityNameFromSearchBarAndSet();

    return view;
  }

  @Override
  public void onDestroyView() {
    unbinder.unbind();
    super.onDestroyView();

  }

  @Override
  public void onCreate(Bundle savedInstanceState) {

    ((App) Objects.requireNonNull(getActivity()).getApplication()).getWeatherComponent()
        .injectWeather(this);

    super.onCreate(savedInstanceState);
    setRetainInstance(true);


  }

  private void getCityNameFromSearchBarAndSet() {
    citySearchView.setOnQueryTextListener(new OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String query) {

        boolean isConnected = isConnectedCheck(Objects.requireNonNull(getContext()));

        if (isConnected) {
          setViewInvisible();
          cityName = citySearchView.getQuery().toString();
          getWeatherInfo();
          citySearchView.clearFocus();
        } else {

          snackbarNetworkConnection(getString(R.string.checkConnection));

        }
        return false;
      }

      @Override
      public boolean onQueryTextChange(String newText) {
        return false;
      }
    });
  }

  @SuppressLint("ResourceType")
  private void snackbarNetworkConnection(String status) {

    final Snackbar checkConnectionSnack = Snackbar
        .make(Objects.requireNonNull(getView()), status,
            Snackbar.LENGTH_LONG);
    checkConnectionSnack.setAction(
        "close", v -> checkConnectionSnack.dismiss());
    checkConnectionSnack.setActionTextColor(Color.parseColor(getString(R.color.lightBlueColor)));
    View sbView = checkConnectionSnack.getView();
    sbView.setBackgroundColor(Color.parseColor(getString(R.color.blackColor)));
    TextView sbTextView = sbView.findViewById(R.id.snackbar_text);
    sbTextView.setTextColor(Color.parseColor(getString(R.color.lightBlueColor)));
    checkConnectionSnack.show();

  }

  @SuppressLint("CheckResult")
  private void getWeatherInfo() {
//    Observable<WeatherResponse> weatherResponseCall = mRetrofit.create(WeatherService.class)
//        .getCurrentWeatherData(cityName);

    mRetrofit.create(WeatherService.class).getCurrentWeatherData(cityName)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<WeatherResponse>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(WeatherResponse weatherResponse) {
            setWeatherInfo(weatherResponse);
            setViewVisible();
          }

          @Override
          public void onError(Throwable e) {

            snackbarNetworkConnection(getString(R.string.wrongCity));
            searchProgressBar.setVisibility(View.INVISIBLE);

          }

          @Override
          public void onComplete() {

          }
        });

//    weatherResponseCall.enqueue(new Callback<WeatherResponse>() {
//
//      @Override
//      public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
//
//        if (response.isSuccessful()) {
//
//          WeatherResponse weatherResponseBody = response.body();
//
//          setWeatherInfo(weatherResponseBody);
//
//          setViewVisible();
//        } else {
//
//          snackbarNetworkConnection(getString(R.string.wrongCity));
//          searchProgressBar.setVisibility(View.INVISIBLE);
//        }
//
//      }
//
//      @Override
//      public void onFailure(Call<WeatherResponse> call, Throwable t) {
//
//        Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
//        searchProgressBar.setVisibility(View.INVISIBLE);
//
//
//      }
//    });
  }


  @SuppressLint("SetTextI18n")
  private void setWeatherInfo(WeatherResponse weatherResponseBody) {
    cityTextView.setText(
        Objects.requireNonNull(weatherResponseBody).getName() + ", " + weatherResponseBody.getSys()
            .getCountry());
    setWeatherIcon(weatherResponseBody.getWeather().get(0).getId(),
        weatherResponseBody.getSys().getSunrise(), weatherResponseBody.getSys().getSunset());
    weatherTextView.setText(weatherResponseBody.getWeather().get(0).getMain());
    humidityTextView
        .setText(
            getString(R.string.humidity_text) + " " + (weatherResponseBody.getMain()
                .getHumidity())
                .toString() + "%");
    pressureTextView
        .setText(
            getString(R.string.pressure_text) + " " + (weatherResponseBody.getMain()
                .getPressure())
                .toString() + " " + getString(R.string.hPa));
    temperatureTextView
        .setText(weatherResponseBody.getMain().getTemp().toString() + " " + getString(
            R.string.celsius));
  }


  private void setWeatherIcon(Integer actualId, long sunrise, long sunset) {
    String icon;
    String pathString;
    long currentTime = new Date().getTime() / 1000;
    if (currentTime >= sunrise && currentTime < sunset) {
      pathString = "wi_owm_day_" + actualId;
    } else {
      pathString = "wi_owm_night_" + actualId;
    }
    int resId = getResources().getIdentifier(pathString, "string", Objects
        .requireNonNull(getContext()).getPackageName());
    icon = getString(resId);
    unicodeIconTextView.setText(icon);
  }


  private void setViewVisible() {
    searchProgressBar.setVisibility(View.INVISIBLE);
    cityTextView.setVisibility(View.VISIBLE);
    unicodeIconTextView.setVisibility(View.VISIBLE);
    humidityTextView.setVisibility(View.VISIBLE);
    pressureTextView.setVisibility(View.VISIBLE);
    temperatureTextView.setVisibility(View.VISIBLE);
  }

  private void setViewInvisible() {
    searchProgressBar.setVisibility(View.VISIBLE);
    cityTextView.setVisibility(View.INVISIBLE);
    unicodeIconTextView.setVisibility(View.INVISIBLE);
    humidityTextView.setVisibility(View.INVISIBLE);
    pressureTextView.setVisibility(View.INVISIBLE);
    temperatureTextView.setVisibility(View.INVISIBLE);
  }


}
